Name:           jlisher-fedora-repo
Version:        0.1.2
Release:        1%{?dist}
Summary:        RPM repository
BuildArch:      noarch

License:        GPLv3+
URL:            https://gitlab.com/jlisher/%{name}/

Source0:        https://sources.jlisher.com/%{name}/%{name}-%{version}.tar.gz
Source1:        https://sources.jlisher.com/%{name}/%{name}-%{version}.tar.gz.asc
Source2:        https://sources.jlisher.com/%{name}/%{name}-%{version}.tar.gz.sha256sum
Source3:        https://sources.jlisher.com/%{name}/%{name}-%{version}.tar.gz.sha256sum.asc
Source4:        https://sources.jlisher.com/gpgkey-17C57CB63CAF20BECB90B9CC4C930D5B79EBABCA.gpg
Source5:        https://sources.jlisher.com/gpgkey-17C57CB63CAF20BECB90B9CC4C930D5B79EBABCA.gpg.sha256sum

BuildRequires:  gnupg2
Requires:       (coreutils or coreutils-single)
Requires:       fedora-repos
Requires:       jlisher-gpg-keys >= %{version}-%{release}

%description
RPM repository

%package -n jlisher-gpg-keys
Summary:        jlisher Packaging RPM keys

%description -n jlisher-gpg-keys
This package provides the RPM signature keys.

%prep
# make sure everything runs successfully
set -euo pipefail

# move into the source directory to verify files
pushd %{_sourcedir}

# Verify GPG keyring checksum (consistency)
sha256sum -c %{SOURCE5}

# Verify tarball checksum GPG signature (authenticity)
%{gpgverify} --keyring='%{SOURCE4}' --signature='%{SOURCE3}' --data='%{SOURCE2}'

# Verify tarball checksum (consistency)
sha256sum -c %{SOURCE2}

# Verify tarball GPG signature (authenticity)
%{gpgverify} --keyring='%{SOURCE4}' --signature='%{SOURCE1}' --data='%{SOURCE0}'

# leave the source directory
popd

# Run setup macro
%setup -q

%build

%install
# make sure everything runs successfully
set -euo pipefail

# create the required directory structure
install -Z -m 755 -d %{buildroot}%{_sysconfdir}/{yum.repos.d,pki/rpm-gpg}

# install package files
install -Z -t %{buildroot}%{_sysconfdir}/yum.repos.d -m 644 yum.repos.d/jlisher-fedora.repo

install -Z -t %{buildroot}%{_sysconfdir}/pki/rpm-gpg -m 644 rpm-gpg/RPM-GPG-KEY-jlisher-packaging

# install GPG key symlinks
pushd %{buildroot}%{_sysconfdir}/pki/rpm-gpg
ln -snr RPM-GPG-KEY-jlisher-packaging RPM-GPG-KEY-jlisher-fedora-35
ln -snr RPM-GPG-KEY-jlisher-packaging RPM-GPG-KEY-jlisher-fedora-36
popd

%check
# make sure everything runs successfully
set -euo pipefail

# Make sure the repo is disabled
! (grep -q 'enabled=1' %{buildroot}%{_sysconfdir}/yum.repos.d/jlisher-fedora.repo) || {
    echo "ERROR: Repo jlisher-fedora should have been disabled, but it isn't"
    exit 1
}

# Check key files exist
pushd %{buildroot}%{_sysconfdir}/pki/rpm-gpg
TMP_KEYRING="$(mktemp)"
for key in "packaging" "fedora-35" "fedora-36"; do
  truncate --size 0 "${TMP_KEYRING}"
  gpg --no-default-keyring --keyring "${TMP_KEYRING}" --import "${PWD}/RPM-GPG-KEY-jlisher-${key}"
  gpg --no-default-keyring --keyring "${TMP_KEYRING}" --list-keys "17C57CB63CAF20BECB90B9CC4C930D5B79EBABCA"
done
rm -f "${TMP_KEYRING}"
popd

%files
%config(noreplace) %{_sysconfdir}/yum.repos.d/jlisher-fedora.repo
%license LICENSE

%files -n jlisher-gpg-keys
%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-jlisher-packaging
%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-jlisher-fedora-35
%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-jlisher-fedora-36

%changelog
* Mon Apr 25 2022 Jarryd Lisher <jarryd@jlisher.com> - 0.1.2-1
- Update Docs
- Add notice to enable the repo
- Add sudo support
- Import GPG Keys
- Add error trap and cleanup script output
- Update support for package managers
- Update GPG verification
* Fri Apr 22 2022 Jarryd Lisher <jarryd@jlisher.com> - 0.1.1-1
- Update spec file
- Update GPG checks
- Update curl requests
- Update README.md
* Fri Apr 22 2022 Jarryd Lisher <jarryd@jlisher.com> - 0.1.0-2
- Initial release
